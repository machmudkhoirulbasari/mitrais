﻿using Service.Register.RegisterService;

namespace Service.Register
{
    public partial class RegisterManager : IRegisterManager
    {
        #region Fields and Constructor
        private readonly IRegisterService registerService;
        public RegisterManager(
            IRegisterService RegisterService
            )
        {
            this.registerService = RegisterService;
        }
        #endregion

        #region Getters
        public IRegisterService RegisterService
        {
            get { return registerService; }
        }
        #endregion
    }
}
