﻿using Service.Register.RegisterService;

namespace Service.Register
{
    public partial interface IRegisterManager
    {
        IRegisterService RegisterService { get; }

    }
}
