﻿using System.Threading.Tasks;
using Core.Contract;
using Core.Object;

namespace Service.Register.RegisterService
{
    public partial interface IRegisterService
    {
        Task<ServiceResult> RegisterAsync(RegisterContract user);
    }
}
