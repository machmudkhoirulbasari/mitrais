﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Core.Contract;
using Core.Helper;
using Core.Object;
using Data;
using Data.Database;
using Newtonsoft.Json;
using System.Globalization;

namespace Service.Register.RegisterService
{
    public partial class RegisterService : IRegisterService
    {
        #region Fields and Constructor
        private readonly IRepository<USER_Register> userRegisterRepository;

        public RegisterService(
            IRepository<USER_Register> userRegisterRepository)
        {
            this.userRegisterRepository = userRegisterRepository;
        }
        #endregion

        #region Methods
        public async Task<ServiceResult> RegisterAsync(RegisterContract register)
        {
            ServiceResult result = new ServiceResult();
            try
            {
                var check = await userRegisterRepository.GetCountAsync(userRegisterRepository.GetTable.Where(d => d.Email.Equals(register.Email) || d.MobileNumber.Equals(register.MobileNumber)).AsQueryable());
                bool? gender;
                DateTime? dob;
                dob = DateTime.ParseExact(register.DateofBirth, "yyyy-M-d", CultureInfo.InvariantCulture);

                if (check == 0)
                {
                    if (!string.IsNullOrEmpty(register.Gender))
                    {
                        gender = bool.Parse(register.Gender);
                    }
                    else
                    {
                        gender = null;
                    }
                    USER_Register data = new USER_Register
                    {
                        FirstName = register.FirstName,
                        LastName = register.LastName,
                        Email = register.Email,
                        CreateDate = DateTime.Now,
                        Gender = gender,
                        DateOfBirth = dob,
                        MobileNumber = register.MobileNumber
                    };
                    var save = await userRegisterRepository.AddAsync(data);
                    result = save.status;
                }
                else
                    result.BadRequest("Data Already Exsits");
                return result;
            }
            catch (Exception ex)
            {
                result.Error("ERROR", ex.Message);
            }
            return result;
        }

        #endregion
    }
}
