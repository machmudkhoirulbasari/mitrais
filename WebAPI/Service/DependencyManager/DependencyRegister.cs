﻿using Autofac;
using Data;
using Service.Register;


namespace Service.DependencyManager
{
    public static partial class DependencyRegister
    {
        public static IContainer CreateDependency()
        {
            var builder = new ContainerBuilder();
            // global register
            builder.RegisterType<RegisterManager>().As<IRegisterManager>();
            builder.RegisterGeneric(typeof(GenericRepository<>)).As(typeof(IRepository<>));

            builder = GetRegisterDependency(builder);

            IContainer Container = builder.Build();
            return Container;
        }
    }
}
