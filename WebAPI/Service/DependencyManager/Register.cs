﻿using Autofac;
using Service.Register.RegisterService;

namespace Service.DependencyManager
{
    public static partial class DependencyRegister
    {
        private static ContainerBuilder GetRegisterDependency(ContainerBuilder builder)
        {
            // register
            builder.RegisterType<RegisterService>().As<IRegisterService>();
            return builder;
        }
    }
}
