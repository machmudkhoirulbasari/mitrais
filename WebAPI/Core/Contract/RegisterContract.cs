﻿using System;
using System.Runtime.Serialization;

namespace Core.Contract
{
    [DataContract]
    public class RegisterContract
    {
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string MobileNumber { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string DateofBirth { get; set; }
        
    }
}
