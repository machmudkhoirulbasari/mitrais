﻿$(document).ready(function () {
    initialize();
});

function initialize() {

    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var qntYears = 35;
    var selectYear = $("#year");
    var selectMonth = $("#month");
    var selectDay = $("#day");
    var currentYear = new Date().getFullYear();

    for (var y = 0; y < qntYears; y++) {
        let date = new Date(currentYear);
        var yearElem = document.createElement("option");
        yearElem.value = currentYear;
        yearElem.textContent = currentYear;
        selectYear.append(yearElem);
        currentYear--;
    }

    for (var m = 0; m < 12; m++) {
        let monthNum = new Date(2018, m).getMonth();
        let month = monthNames[monthNum];
        var monthElem = document.createElement("option");
        monthElem.value = monthNum + 1;
        monthElem.textContent = month;
        selectMonth.append(monthElem);
    }

    var d = new Date();
    var month = d.getMonth();
    var year = d.getFullYear();
    var day = d.getDate();

    selectYear.val(year);
    selectYear.on("change", AdjustDays);
    selectMonth.val(month);
    selectMonth.on("change", AdjustDays);

    AdjustDays();
    selectDay.val(day);

    function AdjustDays() {
        var year = selectYear.val();
        var month = parseInt(selectMonth.val()) + 1;
        selectDay.empty();

        //get the last day, so the number of days in that month
        var days = new Date(year, month, 0).getDate();

        //lets create the days of that month
        for (var d = 1; d <= days; d++) {
            var dayElem = document.createElement("option");
            dayElem.value = d;
            dayElem.textContent = d;
            selectDay.append(dayElem);
        }
    }

    $('#btnRegister').css('color', 'white');
    $('#btnFooter').css('color', 'white');
    $('#btnRegister').css("background-color", "purple");
    $('#btnFooter').css("background-color", "purple");

    HideAll();

    $('#btnRegister').on('click', function () {
        AddRegister();
    });



}

function HideAll() {
    $('#lblEmail').hide();
    $('#lblLastName').hide();
    $('#lblFirstName').hide();
    $('#lblMobileNumber').hide();
    $('#lblRegister').hide();
    $('#lblRegister').text("");
    $('#lblRegister').hide();
}

function ValidateMobileNumber(inputText) {
    var mobileFormat = /^(^\+62\s?|^0)(\d{3,4}-?){2}\d{3,4}$/g;
    if (inputText.match(mobileFormat)) {
        return true;
    }
    else {
        return false;
    }
}
function ValidateFirstName(inputText) {
    if (inputText != '') {
        return true;
    }
    else {
        return false;
    }
}
function ValidateLastName(inputText) {

    if (inputText != '') {
        return true;
    }
    else {
        return false;
    }
}
function ValidateEmail(inputText) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.match(mailformat)) {
        return true;
    }
    else {
        return false;
    }
}

function AddRegister() {

    var email = $('#Email').val();
    var mobileNumber = $('#MobileNumber').val();
    var firstName = $('#FirstName').val();
    var lastName = $('#LastName').val();
    var gender = $("input[name='gender']:checked").val();
    if (gender == 1) {
        gender = true;
    } else if(gender == 0) {
        gender = false;
    }
    var x = $("#month").val();
    var y = $("#day").val();
    var z = $("#year").val();
    var dob = z.concat('-', x, '-', y);

    var formData = new FormData();
    formData.append('FirstName', firstName );
    formData.append('LastName', lastName);
    formData.append('MobileNumber', mobileNumber);
    formData.append('Email', email);
    formData.append('Gender', gender);
    formData.append('DateofBirth', dob);
    
    var Validate = function () {

        var result = true;

        if (ValidateEmail(email) == false) {
            $('#lblEmail').text("Please enter valid email address");
            $('#lblEmail').show();
            result = false;
        } else {
            $('#lblEmail').hide();
        };

        if (ValidateMobileNumber(mobileNumber) == false) {
            $('#lblMobileNumber').text("Please enter valid mobile number");
            $('#lblMobileNumber').show();
            result = false;
        } else {
            $('#lblMobileNumber').hide();
        };

        if (ValidateFirstName(firstName) == false) {
            $('#lblFirstName').text("Please enter valid first name");
            $('#lblFirstName').show();
            result = false;
        } else {
            $('#lblFirstName').hide();
        };

        if (ValidateLastName(lastName) == false) {
            $('#lblLastName').text("Please enter valid last name");
            $('#lblLastName').show();
            result = false;
        } else {
            $('#lblLastName').hide();
        };

        return result;
    };

    if (Validate() === true) {
        $.ajax({
            async: true,
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false,
            url: window.location.origin + "/Account/Register",
            data: formData,
            beforeSend: function () {
            },
            error: function () {

            },
            success: function (data) {
                if (data.succeeded) {
                    $("#FirstName").attr("disabled", "disabled");
                    $("#MobileNumber").attr("disabled", "disabled");
                    $("#LastName").attr("disabled", "disabled");
                    $("#year").attr("disabled", "disabled");
                    $("#day").attr("disabled", "disabled");
                    $("#month").attr("disabled", "disabled");
                    $("#genderFemale").attr("disabled", "disabled");
                    $("#genderMale").attr("disabled", "disabled");
                    $("#Email").attr("disabled", "disabled");
                    $("#btnRegister").attr("disabled", "disabled");
                    $("#btnFooter").attr("value", "Login");
                    $('#lblRegister').text("Data has been saved");
                    $('#lblRegister').show();
                    $("#btnFooter").click(function () {
                        $("#btnFooter").attr("value", "Save");
                        window.location.href = window.location.origin + "/Account/Login";
                    });
                } else
                {
                    $('#lblRegister').text(data.message);
                    $('#lblRegister').show();
                }
            }
        });
    } else
    {
    }
}
