﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MitraisTest.Models;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Net.Http;

namespace MitraisTest.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Obsolete]
        public async Task<JsonResult> Register(Register model)
        {
            ServiceResult result = new ServiceResult();
            //if (ModelState.IsValid)
            //{
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(ConfigurationSettings.AppSettings["BaseUrlApi"].ToString());

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string Jsons = JsonConvert.SerializeObject(model);
                var content = new StringContent(Jsons, Encoding.UTF8, "application/json");
                // content.Headers.Add("Accept", "application/json");
                var res = await client.PostAsync("RegisterService/json/register", content);


                if (res.IsSuccessStatusCode)
                {
                    var RegisterResponse = res.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<ServiceResult>(RegisterResponse);
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    result.Error("Error", "Please Check API !");
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            //}
            //else
            //{
            //    result.Error("Error", "Please Check Input Data");
            //    return Json(result, JsonRequestBehavior.AllowGet);
            //}
        }

        JsonSerializerSettings microsoftDateFormatSettings = new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
        };

    }
}