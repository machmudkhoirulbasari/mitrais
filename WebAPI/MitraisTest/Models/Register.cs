﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MitraisTest.Models
{
    public class Register
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string DateofBirth { get; set; }
    }
}