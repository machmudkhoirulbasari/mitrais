﻿using System;
using System.Threading.Tasks;
using Core.Contract;
using Core.Object;
using WebAPI.Areas.API.Helper;
using System.Runtime.ExceptionServices;
using Service;

namespace WebAPI.Areas.API
{

    public partial class RegisterService : IRegisterAPI
    {

        public IAsyncResult BeginRegister(string format, RegisterContract register, AsyncCallback callback, object asyncState)
        {
            return APIHelper.WrapperService(format, callback, asyncState, true, delegate()
            {
                return ServiceManager.Instance.RegisterManager.RegisterService.RegisterAsync(register);
            });
        }

        public ServiceResult EndRegister(IAsyncResult result)
        {
            try
            {
                return ((Task<ServiceResult>)result).Result;
            }
            catch (AggregateException ex)
            {
                ExceptionDispatchInfo.Capture(ex.InnerException).Throw();
                throw;
            }
        }
    }
}
