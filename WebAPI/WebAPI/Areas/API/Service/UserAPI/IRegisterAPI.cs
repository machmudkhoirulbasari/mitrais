﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ComponentModel;
using Core.Contract;
using Core.Object;

namespace WebAPI.Areas.API
{
    [ServiceContract]
    public partial interface IRegisterAPI
    {
        [OperationContractAttribute(AsyncPattern = true)]
        [WebInvoke(UriTemplate = "{format}/register",
               Method = "POST",
               ResponseFormat = WebMessageFormat.Json,
               BodyStyle = WebMessageBodyStyle.Bare),
        Description("Register User")]
        IAsyncResult BeginRegister(string format, RegisterContract register, AsyncCallback callback, object asyncState);
        ServiceResult EndRegister(IAsyncResult result);

       
    }
}
